#include <Magick++.h> 
#include <iostream> 
#include <string>
#include <vector>
#include <algorithm>
#include <complex>

// using namespace std; 
using namespace Magick; 

typedef std::complex<double> Complex;

std::string to_string(double value, int ndigits)
{
  size_t dotPos = std::to_string(value).find('.');
  std::string result = std::to_string(value).substr(0, dotPos + 3);

  return result;
}
int mandelbrot(double real, double imag) {
	int limit = 100;
	double zReal = real;
	double zImag = imag;

	for (int i = 0; i < limit; ++i) {
		double r2 = zReal * zReal;
		double i2 = zImag * zImag;
		
		if (r2 + i2 > 4.0) return i;

		zImag = 2.0 * zReal * zImag + imag;
		zReal = r2 - i2 + real;
	}
	return limit;
}

unsigned int mandelbrot(Complex c, unsigned int MAX_ITER=100)
{
  Complex z{0.0, 0.0};
  unsigned int n = 0;

  while (fabs(z) <= 2.0 && n < MAX_ITER)
  {
    z = z*z + c;
    n += 1;
  }
  // return n + 1 - log(log2(abs(z))); // smooth coloring
  return n;
}

unsigned int julia(Complex c, Complex z0, unsigned int MAX_ITER=100)
{
  Complex z = z0;
  unsigned int n = 0;

  while (fabs(z) <= 2.0 && n < MAX_ITER)
  {
    z = z*z + c;
    n += 1;
  }
  // return n + 1 - log(log2(abs(z))); // smooth coloring
  return n;
}

unsigned int mandelbrotPower(Complex z0, double M, unsigned int MAX_ITER=100)
{
  Complex z{0.0, 0.0};
  unsigned int n = 0;

  while (fabs(z) <= 2.0 && n < MAX_ITER)
  {
    z = pow(z, M) + z0;
    n += 1;
  }
  return n + 1 - log(log2(abs(z))); // smooth coloring
  // return n;
}

double scale(double val, double vmax, double vmin)
{
    return (val - vmin) / (vmax - vmin);
}

int main(int argc,char **argv) 
{ 
    unsigned int m;
    unsigned int max_it;
    unsigned int width;
    unsigned int height;
    double x_max, x_min, y_max, y_min;
    std::string filename;
    double diff;
    double x0;
    double y0;
    double intensity;
    int color_r;
    int color_g;
    int color_b;

    double hue;

    std::cout << "Initiating program..." << std::endl;

    if (argc < 9)
    {
        std::cout << "Insufficient number of arguments provided. Using default values." << std::endl;

        width = 1920;
        height = 1080;
        max_it = 200;
        x_min = -2.;
        x_max = 2.0;
        y_min = -1.5;
        y_max = 1.5;
        filename = "mandelpow";
    }
    else
    {
        width = atoi(argv[1]);
        height = atoi(argv[2]);
        max_it = atoi(argv[3]);
        x_min = atof(argv[4]);
        x_max = atof(argv[5]);
        y_min = atof(argv[6]);
        y_max = atof(argv[7]);
        filename = argv[8];
    }
    std::cout << "Image width: " << width << std::endl;
    std::cout << "Image height: " << height << std::endl;
    std::cout << "Maximum number of iterations: " << max_it << std::endl;
    std::cout << "X min: " << x_min << std::endl;
    std::cout << "X max: " << x_max << std::endl;
    std::cout << "Y min: " << y_min << std::endl;
    std::cout << "Y max: " << y_max << std::endl;
    std::cout << "File name: " << filename << std::endl;

    InitializeMagick(*argv);

    // Construct the image object. Seperating image construction from the 
    // the read operation ensures that a failure to read the image file 
    // doesn't render the image object useless. 
    Image image(Geometry(width, height), "white");
    // image.magick("SVG");
    // image.magick( "PNG" ); // Set JPEG output format 


    // Complex c(0.285, 0.01);
    // # Other interesting values:
    // # c = complex(-0.7269, 0.1889)
    // # c = complex(-0.8, 0.156)
    // # c = complex(-0.4, 0.6)
    
    // Complex c(0.285, 0.01);
    // Complex c(-0.8, 0.156);
    // Complex c(-0.4, 0.6);
    // Complex c(-0.7269, 0.1889);

    // std::vector<double> M_LIST {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 2.1, 2.3, 2.5 , 2.7, 2.9};
    std::vector<double> M_LIST;

    int nframes = 451;
    for( int i = 0; i < nframes; i++)
    {
        M_LIST.push_back(1+ 0.02*i);
    }
    // -1, -2, -3, -4, -5, -6, -7, -8, -9, 10};
    double M;
    std::string filename_new;

    for(int i = 0; i < nframes; i++)
    {

    // filename_new =  filename + "_" +  std::to_string(M) + ".jpg";
    filename_new = "mandelpow_" + std::to_string(i) + ".jpg";

    for (unsigned int y = 0; y < height; ++y)
    {
        for (unsigned int x = 0; x < width; ++x)
        {
            x0 = x_min + (x_max - x_min) * scale(x, width, 0);
            y0 = y_min + (y_max - y_min) * scale(y, height, 0);

            Complex z0(x0, y0);
            // m = mandelbrot(c, max_it);
            // m = julia(c, z0, max_it);
            // m = mandelbrot(c.real(), c.imag());
            M =  M_LIST[i];
            m = mandelbrotPower(z0, M, max_it);
            // std::cout << "c= " << c << " m=" << m << std::endl;

            intensity = 1 - double(m) / double(max_it);

            color_r = int(520 / intensity );
            color_g = int(230 / intensity );
            color_b = int(310 / intensity );


            // double hueStart = 0.0;  // Starting hue (red)
            // double hueEnd = 240.0;  // Ending hue (blue)

            // ColorRGB color1(1.0, 0.0, 0.0);  // Red
            // ColorRGB color2(0.0, 0.0, 1.0);  // Blue

            // Map iteration count to color gradient
            double t = static_cast<double>(m) / max_it;
            int r = static_cast<int>(9 * (1 - t) * t * t * t * 255);
            // int g = static_cast<int>(15 * (1 - t) * (1 - t) * t * t * 255);
            int g = static_cast<int>(15 * (1 - t) * (1 - t) * t * t * 255);
            // int b = static_cast<int>(8.5 * (1 - t) * (1 - t) * (1 - t) * t * 255);
            int b = static_cast<int>(10.5 * (1 - t) * (1 - t) * (1 - t) * t * 255);

            // double hue = hueStart + t * (hueEnd - hueStart);
            Color color(ColorRGB(r / 255.0, g / 255.0, b / 255.0));
            // ColorRGB pixelColor = color1.scale(1.0 - t) + color2.scale(t);
            image.pixelColor(x, y, color);

            // image.font("Helvetica");

        }
    }

    image.fillColor(Color("white"));
    image.strokeColor(Color("white"));

    // image.pointSize(20);
    image.fontPointsize(30);

    image.draw(DrawableText(width/2-100, 100, "M = "+ to_string(M, 2)) );

    image.write(filename_new);
    std::cout << "Image " << filename_new << " generated!" << std::endl;

    }
  // try { 
  //   // Read a file into image object 
  //   image.read( "logo:" );

  //   // Crop the image to specified size (width, height, xOffset, yOffset)
  //   image.crop( Geometry(100,100, 100, 100) );

  //   // Write the image to a file 
  //   image.write( "logo.png" ); 
  // } 
  // catch( Exception &error_ ) 
  //   { 
  //     cout << "Caught exception: " << error_.what() << endl; 
  //     return 1; 
  //   } 
  return 0; 
}

//c++ -O2 -o example example.cpp `Magick++-config --cppflags --cxxflags --ldflags --libs`
//c++ `Magick++-config --cxxflags --cppflags` -O2 -o demo demo.cpp \
  `Magick++-config --ldflags --libs` 
//g++ `Magick++-config --cxxflags --cppflags` -O2 -o demo demo.cpp `Magick++-config --ldflags --libs`

//export LD_LIBRARY_PATH=/usr/local/lib
//g++ `Magick++-config --cxxflags --cppflags` -O2 -o demo demo.cpp `Magick++-config --ldflags --libs`